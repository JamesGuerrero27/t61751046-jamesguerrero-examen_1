const app = require('../app')

test('Si el numero es igual 1 cambiara por el numero romano I', () => {
    let value = '1'
    expect(app(value)).toBe('I');
  });

test('Si el numero es igual 500 cambiara por el numero romano D', () => {
    let value = "500";
    expect(app(value)).toBe('D');
  });

test('Si el numero es igual 50 cambiara por el numero romano L', () => {
    let value = "50";
    expect(app(value)).toBe('L');
  });

test('Si el numero es igual 100 cambiara por el numero romano C', () => {
    let value = "100";
    expect(app(value)).toBe('C');
  });

test('Si el numero es igual 5 cambiara por el numero romano V', () => {
    let value = "5";
    expect(app(value)).toBe('V');
  });

test('Si el numero es igual 10 cambiara por el numero romano X', () => {
    let value = "10";
    expect(app(value)).toBe('X');
  });

test('Si el numero es igual 50 cambiara por el numero romano L', () => {
    let value = "50";
    expect(app(value)).toBe('L');
  });

test('Si el numero es igual 12 cambiara por el numero romano X', () => {
    let value = "12";
    expect(app(value)).toBe('X');
  });

test('Si el numero es igual 1000 cambiara por el numero romano M', () => {
    let value = "1000";
    expect(app(value)).toBe('M');
  });

test('Si el numero es igual 128 cambiara por el numero romano C', () => {
    let value = "128";
    expect(app(value)).toBe('C');
  });
