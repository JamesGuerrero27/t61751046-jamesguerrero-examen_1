var resultado;

const number = [1000, 500, 100, 50, 10, 5, 1]

const value = ['M', 'D', 'C', 'L', 'X', 'V', 'I']

const app = function (numero) {
    if (numero) {
        resultado = compararNumeroEnteros(numero)
    } else {
        resultado = "";
    }
    return resultado;
}

function compararNumeroEnteros(valor) {
    let numberArr = despegarNumeros(valor);
    let numeroTraductor = ''

    for (let i = 0; i < number.length; i++) {
        for (let j = 0; j < numberArr.length; j++) {
            if (number[i] == numberArr[j]) {
                numeroTraductor += value[i]
            }
        }
    }

    return numeroTraductor
}

function despegarNumeros(valor) {

    let guardarElValorDelArreglo = []
    let x = ''

    for (let j = 0; j < valor.length - 1; j++) {
        x += '0';
    }

    for (let i = 0; i < valor.length; i++) {

        guardarElValorDelArreglo.push(valor[i] + x)
        x = x.slice(1);

        return guardarElValorDelArreglo;
    }
}

module.exports = app;